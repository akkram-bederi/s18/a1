let trainer = {
	name:`LeBron`,
	age:38,
	friends:{
		hoenn:[`Stephen Curry`,`Kevin Durant`],
		kanto:[`Dwyane Wade`, `Kyrie Irving`],
	},
	pokemon:[`Dialga`,`Palkia`,`Giratina`,`Rayquaza`],
	talk:function(){
		console.log(`${this.pokemon[0]} I choose you!`);
	}
}
console.log(trainer)
trainer.talk();




function pokemon(name,lvl,health){
	this.name = name;
	this.level = lvl;
	this.health = health;
	this.attack = parseInt((lvl*10+100)/3)
	this.faint=function(pokemon){
			const{name}=pokemon;
			console.log(`${this.name} tackled ${name} for ${this.attack} damage! ${name}s health has been reduced to 0`);
			console.log(`${name} has fainted`)
	}
	this.tackle=function(pokemon){
		const{name}=pokemon;
		pokemon.health-=this.attack;
			if(pokemon.health<=0){
				this.faint(pokemon);
				}
			else{
				console.log(`${this.name} tackled ${name} for ${this.attack} damage! ${name}s health has been reduced to ${pokemon.health}`);
				}
		}
	}


let piplup = new pokemon(`Piplup`,2,60);
console.log(piplup);
let bulbasaur = new pokemon(`Bulbasaur`,3,80);
console.log(bulbasaur);
let pikachu = new pokemon(`Pikachu`,5,100);
console.log(pikachu);

pikachu.tackle(piplup);
console.log(piplup)
pikachu.tackle(piplup);

console.log(``);
bulbasaur.tackle(pikachu);
console.log(pikachu)
bulbasaur.tackle(pikachu);
console.log(pikachu)

bulbasaur.tackle(pikachu);


console.log(``);
piplup.tackle(bulbasaur);
console.log(bulbasaur);

piplup.tackle(bulbasaur);



